import mechanize
from bs4 import BeautifulSoup

def getNewsText(p_tags):

	tags = {'script', 'iframe', 'div', 'twitter-widget', 'blockquote'}
	for tag in tags:
		for tag2remove in p_tags(tag):
			tag2remove.decompose()

	p_tags = p_tags.find_all('p')

	news_text = ''
	for p_tag in p_tags:
		news_text += p_tag.text + ' '

	return news_text

def agencyTASS(link):
	response = mechanize.urlopen(link)
	soup = BeautifulSoup(response.read(), 'html.parser')

	news_title = soup.find('h1', {'class': 'news-header__title'}).text

	news_lead = soup.find('div', {'class': 'news-header__lead'}).text

	news_text = soup.find('div', {'class': 'text-content'}).text 

	news = {'agency': 'ТАСС', 'title': news_title, 'lead': news_lead, 'text': news_text}

	return news

def agencyRIA(link):
	response = mechanize.urlopen(link)
	soup = BeautifulSoup(response.read(), 'html.parser')
	# return soup

	news_title = soup.find('h1', {'class': 'article__title'}).text

	# news_lead = soup.find('div', {'class': 'article__text'}).text
	soup_lead = soup.find('div', {'class': 'article__text'})
	for strong_tag in soup_lead.find_all('strong'):
		strong_tag.decompose()

	news_lead = soup_lead.text

	paragraphs = soup.find_all('div', {'class': 'article__text'}) 
	news_text = ''

	print('paragraphs:'+str(paragraphs))

	for paragraph in paragraphs:
		news_text += paragraph.text + ' '

	news = {'agency': 'РИА', 'title': news_title, 'lead': news_lead, 'text': news_text}

	return news

def agencyLenta(link):
	response = mechanize.urlopen(link)
	soup = BeautifulSoup(response.read(), 'html.parser')
	# return soup

	news_title = soup.find('h1', {'itemprop': 'headline'}).text
	# news_lead = soup.find('div', {'class': 'article__text'}).text
	news_lead = soup.find('div', {'itemprop': 'articleBody'}).find('p').text

	p_tags = soup.find('div', {'itemprop': 'articleBody'}).find_all('p')

	news_text = ''

	for p_tag in p_tags:
		news_text += p_tag.text + ' '

	news = {'agency': 'Lenta.ru', 'title': news_title, 'lead': news_lead, 'text': news_text}

	return news

def agencyRBK(link):
	response = mechanize.urlopen(link)
	soup = BeautifulSoup(response.read(), 'html.parser')

	news_title = soup.find('div', {'class': 'article__header__title'}).text
	news_lead = soup.find('div', {'class': 'article__text article__text_free'}).find('p').text

	# p_tags = soup.find('div', {'itemprop': 'articleBody'}).find_all('p')
	p_tags = soup.find('div', {'class': 'article__text article__text_free'})
	

	for tag2remove in p_tags('script'):
		tag2remove.decompose()

	for tag2remove in p_tags('div'):
		tag2remove.decompose()

	# tags2delete = p_tags.div.extract()
	# tags2delete = p_tags.script.extract()

	p_tags = p_tags.find_all('p')

	news_text = ''

	for p_tag in p_tags:
		# print('!!!!!!!!!!!!!!'+str(p_tag))
		news_text += p_tag.text + ' '

	news = {'agency': 'РБК', 'title': news_title, 'lead': news_lead, 'text': news_text}

	return news

def agencyKommersant(link):
	response = mechanize.urlopen(link)
	soup = BeautifulSoup(response.read(), 'html.parser')

	if (soup.find('h1', {'class': 'article_name'}) != None):
		news_title = soup.find('h1', {'class': 'article_name'}).text
	else:
		news_title = soup.find('h1', {'class': 'article_subheader'}).text

	news_lead = soup.find('div', {'class': 'article_text_wrapper'}).find('p').text

	news_text = soup.find('div', {'class': 'article_text_wrapper'}).text

	news = {'agency': 'Коммерсантъ', 'title': news_title, 'lead': news_lead, 'text': news_text}

	return news

def agencyRT(link):
	response = mechanize.urlopen(link)
	soup = BeautifulSoup(response.read(), 'html.parser')

	news_title = soup.find('h1', {'class': 'article__heading article__heading_article-page'}).text
	news_lead = soup.find('div', {'class': 'article__summary article__summary_article-page js-mediator-article'}).text

	news_text = getNewsText(soup.find('div', {'class': 'article__text article__text_article-page js-mediator-article'}))

	news = {'agency': 'RT на Русском', 'title': news_title, 'lead': news_lead, 'text': news_text}

	return news

def agencyInterfax(link):
	response = mechanize.urlopen(link)
	soup = BeautifulSoup(response.read(), 'html.parser')

	if (soup.find('article', {'itemprop': 'articleBody'}) != None):
		p_tags = soup.find('article', {'itemprop': 'articleBody'})
		news_title = soup.find('h1', {'itemprop': 'headline'}).text
		news_lead = soup.find('article', {'itemprop': 'articleBody'}).find('p').text
	else:
		p_tags = soup.find('div', {'class': 'article-text'})
		news_title = soup.find('h1', {'class': 'margin-top-0'}).text
		news_lead = soup.find('div', {'class': 'container-resin article-intro__text'}).text

	for tag2remove in p_tags('script'):
		tag2remove.decompose()

	for tag2remove in p_tags('div'):
		tag2remove.decompose()

	p_tags = p_tags.find_all('p')

	news_text = ''

	for p_tag in p_tags:
		news_text += p_tag.text + ' '

	news = {'agency': 'Интерфакс', 'title': news_title, 'lead': news_lead, 'text': news_text}

	return news

def agencyREN(link):
	response = mechanize.urlopen(link)
	soup = BeautifulSoup(response.read(), 'html.parser')

	news_title = soup.find('h1', {'itemprop': 'headline'}).text
	news_lead = soup.find('div', {'itemprop': 'articleBody'}).find('p').text

	p_tags = soup.find('div', {'itemprop': 'articleBody'})

	# for tag2remove in p_tags('script'):
	# 	tag2remove.decompose()

	# for tag2remove in p_tags('div'):
	# 	tag2remove.decompose()

	p_tags = p_tags.find_all('p')

	news_text = ''

	for p_tag in p_tags:
		news_text += p_tag.text + ' '

	news = {'agency': 'РЕН ТВ', 'title': news_title, 'lead': news_lead, 'text': news_text}

	return news

def agencyIz(link):
	response = mechanize.urlopen(link)
	soup = BeautifulSoup(response.read(), 'html.parser')

	news_title = soup.find('h1', {'itemprop': 'headline'}).text
	news_lead = soup.find('div', {'itemprop': 'articleBody'}).find('p').text

	p_tags = soup.find('div', {'itemprop': 'articleBody'}).find('div')
	p_tags = p_tags.find_all('p')

	news_text = ''
	for p_tag in p_tags:
		news_text += p_tag.text + ' '

	news = {'agency': 'Известия', 'title': news_title, 'lead': news_lead, 'text': news_text}

	return news

def agencyMK(link):
	response = mechanize.urlopen(link)
	soup = BeautifulSoup(response.read(), 'html.parser')

	news_title = soup.find('h1', {'itemprop': 'headline'}).text
	news_lead = soup.find('div', {'itemprop': 'articleBody'}).find('p').text

	# p_tags = soup.find('div', {'itemprop': 'articleBody'}).find('div')

	news_text = getNewsText(soup.find('div', {'itemprop': 'articleBody'}))

	news = {'agency': 'Московский Комсомолец', 'title': news_title, 'lead': news_lead, 'text': news_text}

	return news

def agencyGovMos(link):
	response = mechanize.urlopen(link)
	soup = BeautifulSoup(response.read(), 'html.parser')

	news_title = soup.find('div', {'class': 'newContent'}).find('h1').text
	news_lead = soup.find('div', {'class': 'textContent'}).find('p').text

	news_text = getNewsText(soup.find('div', {'class': 'textContent'}))

	news = {'agency': 'Говорит Москва', 'title': news_title, 'lead': news_lead, 'text': news_text}

	return news

def agencyNewsRu(link):
	response = mechanize.urlopen(link)
	soup = BeautifulSoup(response.read(), 'html.parser')

	news_title = soup.find('h1', {'itemprop': 'headline'}).text
	news_lead = soup.find('div', {'class': 'annotation'}).text

	news_text = getNewsText(soup.find('div', {'itemprop': 'articleBody'}))

	news = {'agency': 'News.ru', 'title': news_title, 'lead': news_lead, 'text': news_text}

	return news

def agencyLifeRu(link):
	response = mechanize.urlopen(link)
	soup = BeautifulSoup(response.read(), 'html.parser')

	news_title = soup.find('h1', {'itemprop': 'headline name'}).text

	if (soup.find('div', {'class': 'longreads-subtitle'}) != None):
		news_lead = soup.find('div', {'class': 'longreads-subtitle'}).text
	elif (soup.find('div', {'class': 'post-page-subtitle'}) != None):
		news_lead = soup.find('div', {'class': 'post-page-subtitle'}).text
	else:
		# https://life.ru/1243698
		news_lead = soup.find('div', {'class': 'content-note js-mediator-article'}).find('p').text

	news_text = getNewsText(soup.find('div', {'class': 'content-note js-mediator-article'}))

	news = {'agency': 'Life.ru', 'title': news_title, 'lead': news_lead, 'text': news_text}

	return news

def agencyUraRu(link):
	response = mechanize.urlopen(link)
	soup = BeautifulSoup(response.read(), 'html.parser')

	news_title = soup.find('h1', {'itemprop': 'headline'}).text

	if (soup.find('p', {'class': 'paragraph unit unit_text_m'}) != None):
		paragraphs = soup.find_all('p', {'class': 'paragraph unit unit_text_m'})
		news_lead = paragraphs[0].text
		news_text = ''
		for paragraph in paragraphs:
			news_text += paragraph.text
		# https://yandex.ru/turbo?text=https%3A%2F%2Fura.news%2Fnews%2F1052400350&d=1&utm_referrer=https%3A%2F%2Fyandex.ru%2Fnew
	else:
		paragraphs = soup.find_all('p', {'class': 'paragraph m3SAVKA m3SAVKA_text_m'})
		news_lead = paragraphs[0].text
		news_text = ''
		for paragraph in paragraphs:
			news_text += paragraph.text
		# https://yandex.ru/turbo?text=https%3A%2F%2Fura.news%2Fnews%2F1052400423&d=1&utm_referrer=https%3A%2F%2Fyandex.ru%2Fnew

	news = {'agency': 'URA.Ru', 'title': news_title, 'lead': news_lead, 'text': news_text}

	return news

def getAgency(name, link):
	if (name == 'ТАСС'):
		return agencyTASS(link)
	elif (name == 'РИА Новости'):
		return agencyRIA(link)
	elif (name == 'Lenta.ru'):
		return agencyLenta(link)
	elif (name == 'РБК'):
		return agencyRBK(link)
	elif (name == 'Коммерсантъ'):
		return agencyKommersant(link)
	elif (name == 'RT на русском'):
		return agencyRT(link)
	elif (name == 'Интерфакс'):
		return agencyInterfax(link)
	elif (name == 'РЕН ТВ'):
		return agencyREN(link)
	elif (name == 'Известия'):
		return agencyIz(link)
	elif (name == 'Московский Комсомолец'):
		return agencyMK(link)
	elif (name == 'Говорит Москва'):
		return agencyGovMos(link)
	elif (name == 'News.ru'):
		return agencyNewsRu(link)
	elif (name == 'Life.ru'):
		return agencyLifeRu(link)
	elif (name == 'URA.Ru'):
		return agencyUraRu(link)
	else:
		return None
		# return 'Sorry, ' + name + ' agency is not supported, yet'
