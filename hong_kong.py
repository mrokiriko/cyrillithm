import json
import codecs
import re
import time

import jellyfish
from ru_soundex.soundex import RussianSoundex
from ru_soundex.distance import SoundexDistance

import xlsxwriter

from functions import *

def triPro(a):
	a = a.lower()
	a = a.replace("'", '')
	a = a.replace('"', '')
	a = a.replace(",", '')
	a = a.replace(".", '')
	a = a.replace(":", '')
	a = re.sub(r'\b\w{1,4}\b', '', a)

	a = a.strip()
	a = " ".join(a.split())

	return a	

def tanimoto(a, b):
	c = [v for v in a if v in b]
	return float(len(c))/(len(a)+len(b)-len(c))

def kirilloto(s1, s2):
	words1 = triPro(s1)
	words2 = triPro(s2)

	words1 = words1.split(' ')
	words2 = words2.split(' ')

	k_sum = 0
	k_count = 0

	for word1 in words1:
		for word2 in words2:
			k = tanimoto(word1, word2)
			k_sum += k
			k_count += 1

	return k_sum / k_count

def soundex_cmp(s1, s2):
	sd = RussianSoundex(delete_first_letter=True)
	sd_dist = SoundexDistance(sd)
	return 1 if sd_dist.distance(s1, s2) == 0 else 0

def soundexer(s1,s2):
	words1 = triPro(s1)
	words2 = triPro(s2)

	w1 = words1.split(' ')
	w2 = words2.split(' ')

	sd = RussianSoundex(delete_first_letter=True)
	
	words1 = []
	words2 = []

	try:
		for word in w1:
			words1.append(sd.transform(word))
		for word in w2:
			words2.append(sd.transform(word))
	except:
		print("it all broke")
		print(str(words1))
		print(str(words2))
		print(str(word))
		print(str(w1))
		print(str(w2))
		print(str(s1))
		print(str(s2))
	



	if (len(words2) > len(words1)):
		t = words1
		words1 = words2
		words2 = t

	k_sum = 0
	k_count = 0

	result = [x for x in words1 if x in words2]
	return 2 * len(result) / (len(words1) + len(words2))

def polyphonizer(s1,s2):
	words1 = triPro(s1)
	words2 = triPro(s2)

	w1 = words1.split(' ')
	w2 = words2.split(' ')

	words1 = []
	words2 = []

	for word in w1:
		words1.append(polyphone().convert(word))
	for word in w2:
		words2.append(polyphone().convert(word))

	if (len(words2) > len(words1)):
		t = words1
		words1 = words2
		words2 = t

	k_sum = 0
	k_count = 0

	result = [x for x in words1 if x in words2]
	# result = set(words1).intersection(set(words2))
	# result = (set(words1) & set(words2))
	# result = [x for x in set(words1) if x in set(words2)]

	return 2 * len(result) / (len(words1) + len(words2))

# file2open = 'ya_news_output_09_10.txt'
# file2open = 'ya_news_outputxAllText.txt'

news_number = 1000
file2open = 'all_news.txt'
project_name = 'poly_all_news_' + str(news_number)

other_per = 0
other_count = 0

right_per = 0
right_count = 0

min_from_same = 10
max_from_diff = 0

coefs_same_news = []
coefs_diff_news = []


time1 = time.time()

# with open(file2open, 'r', encoding='utf-8', errors='ignore') as f:
with open(file2open, 'r', encoding='cp1251', errors='ignore') as f:
	news = json.load(f)


	#news = news[850:news_number]
	#news_number = len(news)


	main_news = news[0]

	# m = len(news)
	# m = 100
	m = news_number

	matrix = [[0 for x in range(m)] for y in range(m)] 
	matrix_right = [[0 for x in range(m)] for y in range(m)] 
	# matrix = []

	for i in range(0, m):
		for j in range(0, m):
			if (i <= j):
				break

			main_news = news[i]
			sub_news = news[j]

			par1 = main_news['text']
			par2 = sub_news['text']

			
			# print('news id: ' + str(main_news['news_id']))
			# print('news source: ' + str(main_news['agency']))

			koef = polyphonizer(par1,par2)
			# koef = soundexer(par1,par2)
			# koef = kirilloto(par1,par2)
			percentage = round(koef * 10000) / 100
			# percentage = koef

			matrix[i][j] = percentage
			# matrix[j][i] = percentage

			# print('Похожесть: ' + str(percentage) + '% ', end='')
			if (main_news['news_id'] == sub_news['news_id']):
				# print ('Та же самая новость!')
				matrix_right[i][j] = 1
				# matrix_right[j][i] = 1
				right_per += percentage
				right_count += 1

				coefs_same_news.append(percentage)

				# Минимальный процент среди одинаковых новостей
				min_from_same = percentage if (min_from_same > percentage) else percentage
			else:
				# print ('')
				other_per += percentage
				other_count += 1

				coefs_diff_news.append(percentage)

				# Максимальныйы процент среди разных новостей
				max_from_diff = percentage if (max_from_diff < percentage) else percentage

			# if (percentage > 100):
			# 	print('original:'+par1)
			# 	print('thief:'+par2)
		print(str(i+1) +'/' + str(m))

# fillExcelMatrix(matrix, matrix_right, 'data_matrix_1')
# print(str(matrix))
time2 = time.time()
print ('function took ' + str((time2-time1)*1000.0) + ' ms')

with open('news_matrix_' + project_name + '.txt', 'w', encoding='utf-8') as f:
    json.dump(matrix, f, ensure_ascii=False)

with open('news_matrix_right_' + project_name + '.txt', 'w', encoding='utf-8') as f:
    json.dump(matrix_right, f, ensure_ascii=False)

with open('news_2columns_' + project_name + '.txt', 'w', encoding='utf-8') as f:
    json.dump([coefs_same_news, coefs_diff_news], f, ensure_ascii=False)


print('average percentage right news: ' + str(right_per/right_count))
print('average percentage wrong news: ' + str(other_per/other_count))

diff = right_per/right_count - other_per/other_count
div = (right_per/right_count) / (other_per/other_count)

print('diff: ' + str(round(diff, 2)))
print('div: ' + str(round(div, 2)))

print('min percentage from same news: ' + str(min_from_same))
print('max percentage from different news: ' + str(max_from_diff))