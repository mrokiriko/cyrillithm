import mechanize
from bs4 import BeautifulSoup
from engine import *
import time
import json


def clean(txt):
	txt = txt.replace('\n', '')
	txt = txt.replace('\r', '')
	txt = txt.replace('\xa0', ' ')
	return txt.strip()

def getPage(link):
	print('news link: ' + link)

	response = mechanize.urlopen(link)
	soup = BeautifulSoup(response.read(), 'html.parser')
	# print(str(soup))
	news_name = soup.find('span', {'class': 'story__head-wrap'}).text
	news_text = soup.find('div', {'class': 'doc__text'}).text
	# print("NAME: " + news_name)
	# print("TEXT: " + news_text)

	sources = soup.find_all('div', {'class': 'story__group'})
	sources.pop(0)
	counter = 1
	news = []

	news_id = hash(time.time())

	for source in sources:
		try:
			if (source.find('div', {'class': 'doc__root-title'}) == None):

				# print (str(counter))
				# print(source)

				source_href = source.find('h2', {'class': 'doc__title'}).find('a').get('href')
				source_href = source_href[0:source_href.find('?utm_source')]

				source_title = source.find('h2', {'class': 'doc__title'}).text
				source_name = source.find('span', {'class': 'doc__agency'}).text
				source_time = source.find('span', {'class': 'doc__time'}).text


				# if (source_name == 'RT на Русском'):			
				print(str(counter) + '. ' + source_title + ' (' + source_name + ', ' + source_time + ') #' + source_href)
				
				try:
					agency_info = getAgency(source_name, source_href)
					

					if (agency_info is not None):

						agency_info['news_id'] = news_id
						agency_info['title'] = clean(agency_info['title'])
						agency_info['lead'] = clean(agency_info['lead'])
						agency_info['text'] = clean(agency_info['text'])
						agency_info['link'] = source_href
						# agency_info['text'] = clean(agency_info['text'])

						print(agency_info)


						# agency_info = clean(agency_info)
						news.append(agency_info)  
					else: 
						print('Sorry, ' + source_name + ' agency is not supported, yet')			

				except Exception as e: 
					print('Everything is not fine' + str(e))
				counter += 1
				
		except:
			continue
	return news




# link = 'https://life.ru/1243698'
# data = agencyLifeRu(link)
# print(data)


response = mechanize.urlopen("https://yandex.ru/news/")
soup = BeautifulSoup(response.read(), 'html.parser')
website = "https://yandex.ru"

results = soup.find_all('td', {'class': 'stories-set__item'})

counter = 0
max_count = 100

news_data = []

for result in results:
	counter += 1
	if counter > max_count:
		break

	link = result.find('h2', {'class': "story__title"}).find('a').get('href')
	
	if not ('https' in link):
		link = website + link

	# print('link: ' + link)
	news_data.extend(getPage(link))

with open('ya_news_output_date.txt', 'w', encoding='utf-8') as f:
    json.dump(news_data, f, ensure_ascii=False)
    # json.dump(news_data, f, ensure_ascii=False, indent=4)








# f = open("ya_news_output.txt", "w+")
# # news_data = str(news_data).replace("'", '"')
# f.write(json.dumps(news_data))
# f.close()
